import React from 'react';

// Function to calculate total salary recursively
const calculateTotalSalary = (node) => {
  let totalSalary = 0;

  if (node.children) {
    node.children.forEach(child => {
      totalSalary += calculateTotalSalary(child);
    });
  } else {
    if (typeof node.salary === 'number') {
      totalSalary += node.salary;
    }
  }

  return totalSalary;
};

// Function to calculate total age recursively
const calculateTotalAge = (node) => {
  let totalAge = 0;
  let childrenCount = 0;

  if (node.children) {
    node.children.forEach(child => {
      const childAge = calculateTotalAge(child);
      if (childAge !== undefined) {
        totalAge += childAge;
        childrenCount++;
      }
    });
  } else {
    if (typeof node.age === 'number') {
      totalAge += node.age;
      childrenCount++;
    }
  }

  return childrenCount === 0 ? 0 : totalAge / childrenCount;
};

// Recursive component to render tree nodes and calculate salary and average age
const TreeNode = ({ node }) => {
  const totalSalary = calculateTotalSalary(node);
  const averageAge = calculateTotalAge(node);
  
  return (
    <div>
      <div>{node.name} Salary: {totalSalary} Age: {averageAge}</div>
      {node.children && (
        <div style={{ marginLeft: '20px' }}>
          {node.children.map((child, index) => (
            <TreeNode key={index} node={child} />
          ))}
        </div>
      )}
    </div>
  );
};

// Main Tree component
const Tree = ({ data }) => (
  <div>
    {data.map((node, index) => (
      <TreeNode key={index} node={node} />
    ))}
  </div>
);

// Usage
const treeData = [
  {
    "name": "Lviv",
    "cityId": "1",
    "children": [
      {
        "name": "Silpo",
        "shopId": "shopId1",
        "children": [
          {
            "name": "Pavlo",
            "age": 27,
            "salary": 27000,
            "managerId": "manager1"
          },
          {
            "name": "Marko",
            "age": 34,
            "salary": 45000,
            "managerId": "manager2"
          }
        ]
      },
      {
        "name": "ATB",
        "shopId": "shopId2",
        "children": [
          {
            "name": "Ivan",
            "age": 18,
            "salary": 18000,
            "managerId": "manager4"
          },
          {
            "name": "Oksana",
            "age": 33,
            "salary": 32000,
            "managerId": "manager5"
          }
        ]
      }
    ]
  },
  {
    "name": "Kyiv",
    "cityId": "2",
    "children": [
      {
        "name": "Silpo",
        "shopId": "shopId3",
        "children": [
          {
            "name": "Marko",
            "age": 22,
            "salary": 17000,
            "managerId": "manager6"
          },
          {
            "name": "Victor",
            "age": 45,
            "salary": 35000,
            "managerId": "manager7"
          }
        ]
      },
      {
        "name": "ATB",
        "shopId": "shopId4",
        "children": [
          {
            "name": "Andriy",
            "age": 21,
            "salary": 38000,
            "managerId": "manager8"
          },
          {
            "name": "Volodymyr",
            "age": 29,
            "salary": 42000,
            "managerId": "manager9"
          }
        ]
      }
    ]
  }
];

const App = () => (
  <div>
    <h1>Store Tree</h1>
    <Tree data={treeData} />
  </div>
);

export default App;
